<?php

function inscrireUtilisateur(string $name, string $firstName,string $pseudo, string $email, string $password): bool {
    $password = password_hash($password, PASSWORD_DEFAULT);

    if ( $pdo = pdo()) {
       
        $requeteInscription = "INSERT INTO utilisateurs
        (name,firstName, pseudo, email, password)
        VALUES (:name, :firstName, :pseudo, :email, :password)";
        $query = $pdo->prepare($requeteInscription);
        $query->bindValue(':name', $name, PDO::PARAM_STR);
        $query->bindValue(':firstName', $firstName, PDO::PARAM_STR);
        $query->bindValue(':pseudo', $pseudo, PDO::PARAM_STR);
        $query->bindValue(':email', $email, PDO::PARAM_STR);
        $query->bindValue(':password', $password, PDO::PARAM_STR);
        $query->execute();
        return true;
    } else {
        return false;
    }
}
